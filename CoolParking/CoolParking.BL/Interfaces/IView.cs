﻿namespace CoolParking.BL.Interfaces
{
    public interface IView
    {
        void PrintBalance();
        void PrintVehicleById(string vehicleId);
        void PrintFreePlaces();
        void PrintLastTransactions();
        void PrintLoggedTransactions();
        void PrintVehicleList();
        void PrintErrorMessage(string message);
        void PrintMessage(string message);
        void PrintMenu();
    }
}
