﻿using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public decimal Sum { get; }
        public DateTime Time { get; }
        public string VehicleId { get; }

        public TransactionInfo(decimal sum, DateTime time, string vehicleId)
        {
            Sum = sum;
            Time = time;
            VehicleId = vehicleId;
        }

        public override string ToString()
        {
            return $"{Time} {VehicleId} {Sum}";
        }
    }
}