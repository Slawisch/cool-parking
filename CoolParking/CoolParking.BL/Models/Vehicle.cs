﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get;}
        public VehicleType VehicleType { get;}
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = IdIsValid(id) ? id : throw new ArgumentException("Id format is not valid.");
            VehicleType = (int)vehicleType < 4 && (int)vehicleType >= 0 ? vehicleType : throw new ArgumentException("Type is not valid.");
            Balance = balance > 0 ? balance : throw new ArgumentException("Balance can't be negative.");
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var rand = new Random();

            string result = (char)rand.Next('A', 'Z') + "" +
                            (char)rand.Next('A', 'Z') +
                            "-" +
                            String.Format($"{rand.Next(0, 10000):d4}") +
                            "-" +
                            (char)rand.Next('A', 'Z') + "" +
                            (char)rand.Next('A', 'Z');

            return result;
        }

        public static bool IdIsValid(string vehicleId)
        {
            var regex = new Regex("^\\b[A-Z]{2}-\\d{4}-\\b[A-Z]{2}$");
            return regex.IsMatch(vehicleId);
        }

        public override string ToString()
        {
            return $"Id: {Id} / Type: {VehicleType} / Balance: {Balance}";
        }
    }
}