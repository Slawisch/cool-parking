﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public string LogPath { get; }
        public void Write(string logInfo)
        {
            using (FileStream fstream = new FileStream(LogPath, FileMode.OpenOrCreate | FileMode.Append))
            {
                byte[] array = System.Text.Encoding.Default.GetBytes(logInfo + "\n");
                fstream.Write(array, 0, array.Length);
            }
        }

        public string Read()
        {
            string textFromFile;

            try
            {
                using (FileStream fstream = File.OpenRead(LogPath))
                {
                    byte[] array = new byte[fstream.Length];

                    fstream.Read(array, 0, array.Length);

                    textFromFile = System.Text.Encoding.Default.GetString(array);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new InvalidOperationException();
            }
            
            return textFromFile;
        }
    }
}