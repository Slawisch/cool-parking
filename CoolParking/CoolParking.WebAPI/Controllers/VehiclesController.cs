﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NJsonSchema;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public List<Vehicle> GetVehicles()
        {
            return _parkingService.GetVehicles().ToList();
        }

        [HttpGet("{id}")]
        public IActionResult GetVehicle(string id)
        {
            if (!Vehicle.IdIsValid(id))
                return new BadRequestResult();

            var result = new JsonResult(_parkingService.GetVehicles().ToList().Find(v => v.Id == id));

            if (result.Value != null)
                return result;
            return new NotFoundResult();

        }

        [HttpPost]
        public IActionResult AddVehicle(object obj)
        {
            var jObj = JToken.Parse(obj.ToString() ?? string.Empty);
            string schemaJson = @"{ 
                'type': 'object',
                'required': ['Id', 'VehicleType', 'Balance'],
                'properties':
                    {
                    'Id': {'type':'string'}, 
                    'VehicleType': {'type':'integer'},
                    'Balance': {'type':'number', 'minimum':0,}
                    }
                }";
            var schema = JsonSchema.FromJsonAsync(schemaJson).Result;
            if (schema.Validate(jObj).Count > 0)
                return new BadRequestResult();

            try
            {
                Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(obj.ToString() ?? string.Empty);
                _parkingService.AddVehicle(vehicle);
                var result = new JsonResult(vehicle) { StatusCode = 201 };
                return result;
            }
            catch
            {
                return new BadRequestResult();
            }

        }

        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            if (!Vehicle.IdIsValid(id))
                return new BadRequestResult();

            try
            {
                _parkingService.RemoveVehicle(id);
                return new NoContentResult();
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }
            catch
            {
                return new BadRequestResult();
            }
        }
    }
}
