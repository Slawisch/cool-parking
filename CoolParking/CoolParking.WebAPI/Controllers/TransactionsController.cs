﻿using System;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using NJsonSchema;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public IActionResult GetLastTransactions()
        {
            string content = "";
            try
            {
                content += _parkingService.GetLastParkingTransactions()?.Select(t => t.ToString())
                    .Aggregate((i, j) => i + "\n" + j);
                
            }
            catch
            {
                // ignored
            }

            return new ContentResult()
            {
                Content = content
            };
        }

        [HttpGet("all")]
        public IActionResult GetLoggedTransactions()
        {
            try
            {
                string content = _parkingService.ReadFromLog();
                return new ContentResult()
                {
                    Content = content
                };
            }
            catch
            {
                return new NotFoundResult();
            }
            
        }

        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicle(object obj)
        {
            var jObj = JToken.Parse(obj.ToString() ?? string.Empty);
            string schemaJson = @"{ 
                'type': 'object',
                'required': ['Id', 'Sum'],
                'properties':
                    {
                    'Id': {'type':'string'}, 
                    'Sum': {'type':'number', 'minimum':0,}
                    }
                }";
            var schema = JsonSchema.FromJsonAsync(schemaJson).Result;
            if (schema.Validate(jObj).Count > 0 || !Vehicle.IdIsValid(jObj["Id"].Value<string>()))
                return new BadRequestResult();

            try
            {
                _parkingService.TopUpVehicle(jObj["Id"].Value<string>(), jObj["Sum"].Value<decimal>());
                return new JsonResult(_parkingService.GetVehicles().ToList()
                    .Find(v => v.Id == jObj["Id"].Value<string>()));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new NotFoundResult();
            }
        }
    }
}
